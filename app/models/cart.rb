class Cart < ActiveRecord::Base
  has_many :item
  
  
  def inc?(item)
  self.item.each do |i|
     if i.product_id == item.id
       return true
     end
   end
   return false
 end
 #The following code is heavily based upon Ryan Bates at railscasts.com
 def paypal_encrypted(return_url, notify_url)
    values = {
      :business => 'test_s_1331012439_biz@aberdeen.ac.uk',
      :cmd => '_cart',
      :upload => 1,
      :return => return_url,
      :invoice => id,
      :currency_code=> 'GBP',
      :notify_url => notify_url,
      :cert_id => 'PA8NDBVB83X2A'
    }
    
    item.each_with_index do |item, index|
      values.merge!({
        "amount_#{index + 1}" => item.pPrice,
        "item_name_#{index + 1}" => item.pName,
        "item_number_#{index + 1}" => item.product_id,
        "quantity_#{index + 1}" => item.quantity
      })
    end
    encrypt_for_paypal(values)
  end
  #Set constants for certs.
PAYPAL_CERT_PEM = File.read("#{Rails.root}/certs/paypal_cert.pem")
APP_CERT_PEM = File.read("#{Rails.root}/certs/app_cert.pem")
APP_KEY_PEM = File.read("#{Rails.root}/certs/app_key.pem")

#Use OpenSSL to encrypt the values. Not nice but functional
def encrypt_for_paypal(values)
  signed = OpenSSL::PKCS7::sign(OpenSSL::X509::Certificate.new(APP_CERT_PEM), OpenSSL::PKey::RSA.new(APP_KEY_PEM, ''), values.map { |k, v| "#{k}=#{v}" }.join("\n"), [], OpenSSL::PKCS7::BINARY)
  OpenSSL::PKCS7::encrypt([OpenSSL::X509::Certificate.new(PAYPAL_CERT_PEM)], signed.to_der, OpenSSL::Cipher::Cipher::new("DES3"), OpenSSL::PKCS7::BINARY).to_s.gsub("\n", "")
end

  
end
