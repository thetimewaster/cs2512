class User < ActiveRecord::Base
  authenticates_with_sorcery!
	has_many :reviews
	attr_accessible :email, :password, :password_confirmation

	  validates_confirmation_of :password
	  validates_presence_of :password, :on => :create
	  validates_presence_of :email
	  validates_uniqueness_of :email
	  ROLES = %w[admin author]
	
	def self.current
	    Thread.current[:user]
	  end
	  def self.current=(user)
	    Thread.current[:user] = user
	  end
end