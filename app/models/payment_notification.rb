class PaymentNotification < ActiveRecord::Base
  belongs_to :Cart
  #This method converts data to YML format when saving to DB and back to HASH when retrieved
  #seralize :params
  after_create :mark_cart_as_purchased
  
  private
  
  def mark_cart_as_purchased
    if status == "Completed" && params[:secret] == "Nerf_Herder"
      cart.update_attribute(:purchased_on, Time.now)
    end
  end
end
