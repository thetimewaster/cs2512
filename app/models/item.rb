class Item < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product
  
  def total
    return self.pPrice.to_f * self.quantity
  end
end
