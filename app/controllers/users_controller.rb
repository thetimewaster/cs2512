class UsersController < ApplicationController
  def new
	@user = User.new
  end

	def create
		@user = User.new(params[:user])
		@user.role = "author"
		if @user.save
			redirect_to root_url, :notice => "You have successfully registered!"
		else
			render :new
		end
	end
end
