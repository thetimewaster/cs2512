class ProductsController < ApplicationController
  # GET /products
  # GET /products.json
  def index
    @products = Product.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  def search
	@products = Product.find(:all, :order => params[:order]+" "+params[:type], :conditions => ["pName LIKE ?", "%#{params[:pName]}%"])
  end
  
  # GET /products/1
  # GET /products/1.json
  def show
  @product = Product.find(params[:id])
	@reviews = Review.where(:rProduct => params[:id])
	session[:productId] = params[:id]

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new
	@categories = Category.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
	@categories = Category.all
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(:pName => params[:pName], :pDescription => params[:pDescription], :pPrice => params[:pPrice], :pPicture => params[:pPicture], :pSpec => params[:pSpec], :pCategory => params[:pCategory])
	if @product.save
		redirect_to :pIndex, :notice => "Product added to site"
	else
		render "new"
	end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])

	if @product.update_attributes(:pName => params[:pName], :pDescription => params[:pDescription], :pPrice => params[:pPrice], :pPicture => params[:pPicture], :pSpec => params[:pSpec], :pCategory => params[:pCategory])
		redirect_to :pIndex, :notice => "Product updated"	
	else
		render edit
	end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :ok }
    end
  end
end
