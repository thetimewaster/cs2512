class CategoriesController < ApplicationController
  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @categories }
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    @category = Category.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @category = Category.new
	@categories = Category.all
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
	@categories = Category.all
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(:cName => params[:cName], :cDescription => params[:cDescription], :cChildOf => params[:cCategory])

	if @category.save
        redirect_to :cIndex, :notice => "Category added to site"
	else
        render new
	end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    @category = Category.find(params[:id])

	if @category.update_attributes(:cName => params[:cName], :cDescription => params[:cDescription], :cChildOf => params[:cCategory])
		redirect_to :cIndex, :notice => "Category updated"
	else
		render edit
	end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
      format.json { head :ok }
    end
  end
end
