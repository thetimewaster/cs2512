class ReviewsController < ApplicationController
	# GET /reviews
  # GET /reviews.json
	authorize_resource :except => [:show,:create]
	def index
    @reviews = Review.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reviews }
    end
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
    @review = Review.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/new
  # GET /reviews/new.json
  def new
    @review = Review.new
	@product = Product.find(session[:productId])
	@products = Product.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/1/edit
  def edit
    @review = Review.find(params[:id])
	@products = Product.all
  end

  # POST /reviews
  # POST /reviews.json
  def create
    @review = Review.new(:rTitle => params[:rTitle], :rBody => params[:rBody], :rPoster => current_user.id, :rProduct => params[:rProduct])

      if verify_recaptcha & @review.save
		redirect_to :controller => "products", :action => "show", :id => session[:productId], :notice => "Review added to product"
      else
		@product = Product.find(session[:productId])
        render "new"
      end
  end

  # PUT /reviews/1
  # PUT /reviews/1.json
  def update
    @review = Review.find(params[:id])

    if @review.update_attributes(:rTitle => params[:rTitle], :rBody => params[:rBody], :rProduct => params[:rProduct])
		redirect_to :rIndex, :notice => "Review updated"	
    else
		render edit
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review = Review.find(params[:id])
    @review.destroy

    respond_to do |format|
      format.html { redirect_to reviews_url }
      format.json { head :ok }
    end
  end
end
