class ItemsController < ApplicationController
  def create
    @pProduct = Product.find(params[:product])
    @current_cart = current_cart
    if (@current_cart != nil && @current_cart.inc?(@pProduct))
      flash[:error] = "Item is already in cart, please update the value below."
    else
    @item = Item.create!(:cart_id => @current_cart.id, :product_id => @pProduct.id, :pName => @pProduct.pName, :quantity => 1, :pPrice => @pProduct.pPrice)
    flash[:notice] = "Successfully added #{@pProduct.pName}"
    end
    redirect_to current_cart_url 
  end
  
  def total
   pPrice * quantity
  end
  
  def update
    if(params[:quantity] > 0)
    self.quantity = params[:quantity]
    elsif(params[:quantity] == 0)
      self.destroy
     elsif(params[:quantity < 0])
       flash[:error] = "Quantity must be 0 or greater."
    end
  end  
end
