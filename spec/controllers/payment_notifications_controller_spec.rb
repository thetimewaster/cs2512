require 'spec_helper'

describe PaymentNotificationsController do
  it "Creates a row based upon outside input." do
    PaymentNotification.create!(:params => "Blah", :cart_id => 1, :status => "Completed", :transaction_id => 1, :secret_key => "Nerf_Herder")
    PaymentNotifications.first.cart_id.should  == 1 
    end

end
