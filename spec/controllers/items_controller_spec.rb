require 'spec_helper'
describe ItemController do
  describe "Calculates the total of an item" do
    Item.first().total.should == 15
  end
end