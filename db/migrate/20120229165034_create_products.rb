class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :pName
      t.text :pDescription
      t.float :pPrice
      t.string :pPicture
      t.text :pSpec
      t.integer :pCategory

      t.timestamps
    end
  end
end
