class CreateReviews < ActiveRecord::Migration
  def change
 #   change_table :reviews do |t|
	#  t.integer :rProduct
  #  end
	
	create_table "reviews", :force => true do |t|
    t.string   "rTitle"
    t.text     "rBody"
    t.integer  "rPoster"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rProduct"
  end

  end
end
