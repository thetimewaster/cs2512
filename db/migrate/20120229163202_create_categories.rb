class CreateCategories < ActiveRecord::Migration
	def change
    create_table :categories do |t|
      t.string :cName
      t.text :cDescription
      t.integer :cChildOf

      t.timestamps
    end
  end
end
