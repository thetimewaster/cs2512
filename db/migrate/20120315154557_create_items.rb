class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.decimal :pPrice
      t.string :pName
      t.integer :cart_id
      t.integer  :product_id
      t.integer :quantity

      t.timestamps
    end
  end
end
