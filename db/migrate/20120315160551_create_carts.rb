class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.date :purchased_on

      t.timestamps
    end
  end
end
