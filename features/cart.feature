Feature: Shopping Cart
	As a User
	I should be able to create a shopping cart
	And add and subtract items as I see fit.
	
	Scenario: Create a new cart for a new user
		Given I do not have a cart
		And I attempt to add an item
		Then a new cart is created